import java.util.*;

import static java.lang.Math.abs;


public class Service {
    public static Collection<Data> dataWithName(List<Data> dataList, String name) {
        List<Data> dataWithNameList = new LinkedList<>();
        
        if (name == null) {
            for (Data data: dataList)
                if (data.getName() == null)
                    dataWithNameList.add(data);
        }
        
        else {
            for (Data data: dataList)
                if (name.equals(data.getName()))
                    dataWithNameList.add(data);
        }
        
        return dataWithNameList;
    }
    
    
    public static Collection<Data> dataWithValueNotGreaterThan(List<Data> dataList, double level) {
        if (Double.compare(level, 0) < 0)
            throw new IllegalArgumentException("Level value cannot be negative");
        
        List<Data> dataWithValueNotGreaterThanLevel = new LinkedList<>();
        
        for (Data data: dataList)
            if (Double.compare(abs(data.getValue()), level) <= 0)
                dataWithValueNotGreaterThanLevel.add(data);
        
        return dataWithValueNotGreaterThanLevel;
    }
    
    
    public static Set<Double> valuesOfDataWithNames(List<Data> dataList, Set<String> names) {
        Set<Double> values = new HashSet<>();
        
        for (Data data: dataList)
            if (names.contains(data.getName()))
                values.add(data.getValue());
        
        return values;
    }
    
    
    public static String[] namesOfDataWithPositiveValue(List<Data> dataList) {
        Set<String> names = new HashSet<>();
        
        for (Data data: dataList)
            if (Double.compare(data.getValue(), 0) > 0)
                names.add(data.getName());
        
        return names.toArray(new String[0]);
    }
    
    
    public static <T> Set<T> unity(List<Set<T>> sets) {
        Set<T> unity = new HashSet<>();
        
        for (Set<T> set: sets)
            unity.addAll(set);
        
        return unity;
    }
    
    
    public static <T> Set<T> intersection(List<Set<T>> sets) {
        Set<T> intersection = new HashSet<>(sets.get(0));
        
        for (int i = 1; i < sets.size(); i++)
            intersection.retainAll(sets.get(i));
        
        return intersection;
    }
    
    
    public static <T> List<Set<T>> maxSizedSets(List<Set<T>> sets) {
        List<Set<T>> maxSizedSets = new LinkedList<>();
        int maxSize = sets.get(0).size();
        
        maxSizedSets.add(sets.get(0));
        
        for (int i = 1; i < sets.size(); i++) {
            Set<T> set = sets.get(i);
            
            if (set.size() == maxSize)
                maxSizedSets.add(set);
            
            else if (set.size() > maxSize) {
                maxSize = set.size();
                maxSizedSets.clear();
                maxSizedSets.add(set);
            }
        }
        
        return maxSizedSets;
    }
}
